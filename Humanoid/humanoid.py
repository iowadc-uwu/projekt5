import gymnasium as gym
import numpy as np
from collections import defaultdict
import tqdm
from stable_baselines3 import SAC, DDPG, A2C, PPO
from stable_baselines3.common.callbacks import CheckpointCallback, CallbackList
import os

from projekt5.Humanoid.callback import PlotLearningCurveCallback, SaveRewardsCallback

def make_env(expected_timesteps):
    env = gym.make("Humanoid-v4")
    env = gym.wrappers.RecordEpisodeStatistics(env, deque_size=expected_timesteps*10)
    return env


def train_model(timesteps, model_name: str, model_nr: int, hyperparameters: dict):
    checkpoint_callback = CheckpointCallback(
        save_freq=min(5000, timesteps),
        save_path=f"./projekt5/Humanoid/logs/{model_name}__{model_nr}",
        name_prefix=model_name,
        save_replay_buffer=True,
        save_vecnormalize=True,
    )

    callback = CallbackList([checkpoint_callback, SaveRewardsCallback("./projekt5/Humanoid/rewards/" + model_name, model_nr)])

    env = gym.make("Humanoid-v4")
    env = gym.wrappers.RecordEpisodeStatistics(env, deque_size=timesteps*500)

    # model = SAC('MlpPolicy', env, verbose=1, gamma=discount_factor)
    # model = DDPG('MlpPolicy', env, verbose=1, gamma=discount_factor)
    model = PPO(
        "MlpPolicy",
        env,
        learning_rate=hyperparameters["learning_rate"],
        n_steps=hyperparameters["n_steps"],
        batch_size=hyperparameters["batch_size"],
        gamma=hyperparameters["gamma"],
        clip_range=hyperparameters["clip_range"],
        ent_coef=hyperparameters["ent_coef"],
        verbose=1
    )
    model.learn(total_timesteps=timesteps, callback=callback)


def train_model_with_hyperparameters(timesteps, model_name: str, hyperparameters: dict):
    save_path = f"./projekt5/Humanoid/rewards/{model_name}"
    for i in range(10):
        if not os.path.exists(save_path) or f'rewards_{i}.npy' not in os.listdir(save_path):
            train_model(timesteps, model_name, i, hyperparameters)


def continue_training(timesteps, model, model_name: str):
    checkpoint_callback = CheckpointCallback(
        save_freq=min(5000, timesteps),
        save_path="./projekt5/Humanoid/logs/" + model_name,
        name_prefix=model_name,
        save_replay_buffer=True,
        save_vecnormalize=True,
    )

    callback = CallbackList([checkpoint_callback, PlotLearningCurveCallback("./projekt5/Humanoid/logs/" + model_name)])

    model.learn(total_timesteps=timesteps, callback=callback)

def test_model(n_episodes, model):
    env = gym.make("Humanoid-v4", render_mode="human")

    for episode in tqdm.tqdm(range(n_episodes)):
        cum_reward = 0
        obs, info = env.reset()
        done = False

        while not done:
            action, _states = model.predict(obs, deterministic=True)
            obs, reward, terminated, truncated, info = env.step(action)
            done = terminated or truncated
            cum_reward += reward

        print(f"Cumulative reward: {cum_reward}")
    env.close()

def load_model(path, env):
    model = PPO.load(path, env)
    return model


hyperparams_set_1 = {
    'learning_rate': 3e-4,
    'n_steps': 2048,
    'batch_size': 64,
    'gamma': 0.98,
    'clip_range': 0.2,
    'ent_coef': 0.01
}

hyperparams_set_2 = {
    'learning_rate': 1e-3,
    'n_steps': 4096,
    'batch_size': 128,
    'gamma': 0.98,
    'clip_range': 0.2,
    'ent_coef': 0.02
}

hyperparams_set_3 = {
    'learning_rate': 1e-4,
    'n_steps': 1024,
    'batch_size': 32,
    'gamma': 0.98,
    'clip_range': 0.1,
    'ent_coef': 0.005
}

train_model_with_hyperparameters(50_000, "ppo1", hyperparams_set_1)
train_model_with_hyperparameters(50_000, "ppo2", hyperparams_set_2)
train_model_with_hyperparameters(50_000, "ppo3", hyperparams_set_3)

