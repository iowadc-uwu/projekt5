import numpy as np
import matplotlib.pyplot as plt
import os

def create_curve(model_name, x=100):
    rewards_path = f"./projekt5/Humanoid/rewards/{model_name}" 

    all_rewards = []
    all_timesteps = []

    for i in range(10):
        rewards = np.load(rewards_path + f'/rewards_{i}.npy')
        timesteps = np.load(rewards_path + f'/timesteps_{i}.npy')
        all_rewards.append(rewards.ravel())
        all_timesteps.append(timesteps)

    # Define common timesteps
    common_timesteps = np.arange(0, min([ts[-1] for ts in all_timesteps]), x)

    # Interpolate rewards to common timesteps
    interpolated_rewards = []
    for rewards, timesteps in zip(all_rewards, all_timesteps):
        interpolated_rewards.append(np.interp(common_timesteps, timesteps, rewards))

    # Calculate mean and standard deviation
    mean_rewards = np.mean(interpolated_rewards, axis=0)
    std_rewards = np.std(interpolated_rewards, axis=0)

    # Plot mean rewards with standard deviation
    plt.figure(figsize=(10, 6))
    plt.plot(common_timesteps, mean_rewards, label='Mean Reward')
    plt.fill_between(common_timesteps, mean_rewards - std_rewards, mean_rewards + std_rewards, alpha=0.3, label='Std Dev')
    plt.xlabel('Timesteps')
    plt.ylabel('Rewards')
    plt.title('Mean Rewards by Timesteps with Std Dev')
    plt.legend()
    plt.grid(True)
    os.makedirs(f'./projekt5/Humanoid/curves', exist_ok=True)
    plt.savefig(f'./projekt5/Humanoid/curves/mean_rewards_by_timesteps_{model_name}.png')
    plt.show()


create_curve("ppo1")
create_curve("ppo2")
create_curve("ppo3")
